--SELECT count(*) from twist_twittertrend WHERE twist_twittertrend.name LIKE '%maisvoce%';

-- SELECT * from  twist_twittertrend ORDER BY twist_twittertrend.collected_at DESC;

select 
        name AS hashtag, 
        query, 
        url,
        collected_at, 
        collected_at at time zone 'america/sao_paulo' at time zone 'utc' AS localtime, 
        trend_order,
        language
from twist_twittertrend 
where
twist_twittertrend.name LIKE '%maisvoce%'
AND twist_twittertrend.collected_at BETWEEN '2017-12-31' AND '2019-01-01';